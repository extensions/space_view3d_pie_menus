# 3D Viewport Pie Menus

This extension (available on the <a href="https://extensions.blender.org/add-ons/viewport-pie-menus/">Blender Extensions</a> platform) adds various pie menus to facilitate speedy, gesture based workflows.
Each pie menu has a default shortcut, which can be customized or disabled in the preferences.

<img src="docs/preferences.png" width=600>

## Installation

- Open Blender and go to Edit->Preferences->Extensions.
- Search for "3D Viewport Pie Menus".
- Click Install.

## Non-QWERTY Keyboards
The add-on was designed for QWERTY keyboard layout, and currently cannot auto-adapt to other layouts. You may have to customize the hotkeys to fit your keyboard. However, customizing the hotkeys is very easy through the add-on's preferences!

## Pie Menus

### Editor Switch: `Ctrl + Alt + S`
A nested pie menu to switch to any editor type and sub-type. Even custom Node Editors from add-ons!  
<img src="docs/pie_editor_switch.png" width=600>

### Relationships / Delete: `X`
More accurate distinction between "Unlink" and "Delete". Custom utilities for remapping users of a datablock, and inspecting dependencies. Also works in the Outliner.  
<img src="docs/pie_relationships_delete.png" width=600>

### File: `Ctrl + S + Drag`
Save/Open, Link/Append, Import/Export files. Only appears on drag, so just tapping Ctrl+S will just save your file without a pie menu.  
<img src="docs/pie_save_load.png" width=600>

### Libraries: `File Pie -> Top-Left`
This is a sub-pie of the File Pie, to operate on data coming from other .blend files. (Un)Pack All Local will affect both libraries and images. But note that Blender has limitations when it comes to packing linked data, such as images used by linked objects.  
<img src="docs/pie_libraries.png" width=600>

### Sculpt Brush Select: `W`
Nested pie to set the active sculpt brush asset. All brushes that ship with 4.2 or 4.3 are included. The Asset selector at the bottom is only available in 4.3 and beyond. You can find your custom brushes in there.  
<img src="docs/pie_sculpt_brush_select.png" width=600>

### Camera: `Alt + C`
Camera operations and properties, with improved Snap Camera to View, and a custom New Camera From View operator. Some gestures are context-sensitive based on if you're already inside a camera or not.  
<img src="docs/pie_camera.png" width=600>

### Window: `Ctrl + Space + Drag`
Open and full-screen windows and areas. Only triggered on mouse drag, so tapping Ctrl+Space still maximizes the current editor.  
<img src="docs/pie_window.png" width=600>

### 3D View: `Shift + C + Drag`
Operations related to the 3D View. Only triggered on mouse drag, so tapping Shift+C still frames the whole scene and resets your cursor.  
<img src="docs/pie_3dview.png" width=600>

### Animation: `Shift + Spacebar + Drag`
Jump around in time to various points, such as the start/end of the shot, next/previous keyframes, etc. Only triggered on mouse drag, so tapping Shift+Space still just plays your animation without summoning a pie menu.  
<img src="docs/pie_animation.png" width=600>

### Apply Transforms: `Ctrl + A`
Apply or clear an object's Translation, Rotation, Scale, or all transforms. This may require making the object's data local and single-user, so those functionalities are here too.  
<img src="docs/pie_apply_transforms.png" width=600>

### Manipulators: `Alt + Spacebar`
Toggle the transformation gizmo on and off, either as a whole, or separately for location/rotation/scale.  
<img src="docs/pie_manipulators.png" width=600>

### Set Origin: `Ctrl + Alt + X`
The Set Origin menu as a pie, with a couple of additions: Origin to Selected (works with an edit-mode or object-mode selection), and Origin to Bottom, which sets the origin to the bottom of the bounding box.  
<img src="docs/pie_set_origin.png" width=600>

### Proportional Editing: `O + Drag`
Toggle Proportional Editing settings. Only triggered on mouse drag, so tapping O still just toggles proportional editing.  
<img src="docs/pie_proportional.png" width=700>

### Object Display: `Shift + W`
Object display properties and a menu of shading operators. "Reset Normals" is a new operator that combines a few built-in ones to truly reset an object's normals. This pie is context-sensitive so it will show different options depending on the active object's type.  
<img src="docs/pie_object_display.png" width=700>

### Selection: `A`
Object & Mesh selection operators.  
<img src="docs/pie_object_selection.png" width=800>

<img src="docs/pie_mesh_selection.png" width=800>

### Select By Name: `Ctrl+F`
Select objects by various name-based relationships, such as the opposite side, higher or lower number, or just straight up type a name in a search bar.
<img src="docs/pie_object_select_by_name.png" width=700>

### Parenting: `P`
Object parenting with greatly improved button labels and tooltips to reduce confusion.  
<img src="docs/pie_object_parenting.png" width=600>

### Mesh Delete: `X`
Just the mesh delete menu, but as a pie.  
<img src="docs/pie_mesh_delete.png" width=600>

### Mesh Merge: `M`
Just the mesh merge menu, but as a pie.  
<img src="docs/pie_mesh_merge.png" width=600>

### Mesh Flatten: `Alt + X`
Flatten the selection to various commonly used pivot points, like the object origin or selection bounding box center.
<img src="docs/pie_mesh_flatten.png" width=600>

### Preferences: `Ctrl + U`
Quickly set or reset your preferences or start-up file.  
<img src="docs/pie_preferences.png" width=600>

### Area Split/Join: ```Alt + ` ```
Split or join editor areas quickly. This may become redundant in Blender 4.3.  
<img src="docs/pie_area_split_join.png" width=600>